// ttlmap is a simple key-value store (a map) with an expiration
// attached to each entry.
//
// To actually expire the items, tl.Expire() should be called
// "sometimes".  When to schedule its execution depends on the
// application code.  Otherwise, tl.AutoExpire() is provided too:
// it runs a goroutine that issue tl.Expire() periodically to expire
// the items.
//
// Example usage:
//
//	type state struct { /* ... */ }
//	var userstate = ttlmap.New[int64, state](time.Hour)
//	userstate.AutoExpire()
package ttlmap

import (
	"sync"
	"time"
)

type item[T any] struct {
	value T
	ttl   time.Duration
}

type TTLMap[K comparable, T any] struct {
	// The default key duration.
	TTL time.Duration

	items     map[K]item[T]
	lastcheck time.Time
	rwmtx     sync.RWMutex
}

// New creates a new TTLMap specifying a default TTL for the entries.
func New[K comparable, T any](defttl time.Duration) *TTLMap[K, T] {
	tl := &TTLMap[K, T]{
		items:     make(map[K]item[T]),
		lastcheck: time.Now(),
		TTL:       defttl,
	}
	return tl
}

// AutoExpire schedules a go routine to periodically call Expire
// on the map to remove expired entries.
func (tl *TTLMap[T, K]) AutoExpire() {
	go func() {
		for {
			time.Sleep(tl.TTL * 2)
			tl.Expire()
		}
	}()
}

// Expire removes from the TTLMap the entries that are expired.  It's
// useful for applications that are not using AutoExpire() to control
// when to expire entries.
func (tl *TTLMap[T, K]) Expire() {
	tl.rwmtx.Lock()
	defer tl.rwmtx.Unlock()

	now := time.Now()
	elapsed := now.Sub(tl.lastcheck)
	for key, val := range tl.items {
		if elapsed > val.ttl {
			delete(tl.items, key)
			continue
		}
		val.ttl -= elapsed
	}

	tl.lastcheck = now
}

// Add an item to the TTLMap using the default expiration time.
func (tl *TTLMap[K, T]) Add(key K, value T) {
	tl.Add3(key, value, tl.TTL)
}

// Add an item to the TTLMap with a custom expiration time.
func (tl *TTLMap[K, T]) Add3(key K, value T, ttl time.Duration) {
	tl.rwmtx.Lock()
	defer tl.rwmtx.Unlock()

	tl.items[key] = item[T]{
		value: value,
		ttl:   ttl,
	}
}

// Get retrieves an element from the TTLMap.
func (tl *TTLMap[K, T]) Get(key K) (T, bool) {
	tl.rwmtx.RLock()
	defer tl.rwmtx.RUnlock()

	item, found := tl.items[key]
	return item.value, found
}

// Bump 'bumps' the expiration time by the given duration.
func (tl *TTLMap[K, T]) Bump(key K, ttl time.Duration) {
	tl.rwmtx.Lock()
	defer tl.rwmtx.Unlock()

	if item, found := tl.items[key]; found {
		item.ttl += ttl
	}
}

// Get retrieves an element from the TTLMap and bumps its
// expiration time.  Equivalent to call Get followed by Bump
// but it's atomic.
func (tl *TTLMap[K, T]) GetBump(key K, ttl time.Duration) (T, bool) {
	tl.rwmtx.RLock()
	defer tl.rwmtx.RUnlock()

	item, found := tl.items[key]
	if found {
		item.ttl += ttl
	}
	return item.value, found
}

// Delete removes an element from the TTLMap.
func (tl *TTLMap[K, T]) Delete(key K) {
	tl.rwmtx.Lock()
	defer tl.rwmtx.Unlock()

	delete(tl.items, key)
}
