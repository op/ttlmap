# ttlmap [![Go Documentation](https://godocs.io/go.omarpolo.com/ttlmap?status.svg)](https://godocs.io/go.omarpolo.com/ttlmap)

ttlmap is a simple key-value store (a map) with an expiration attached
to each entry.

```
$ go get go.omarpolo.com/ttlmap
```

To actually expire the items, `tl.Expire()` should be called "sometimes".
When to schedule its execution depends on the application.  Otherwise,
`tl.AutoExpire()` is provided too: it runs a goroutine that issue
`tl.Expire()` periodically to expire the items.

Example usage:

```go
type state struct { /* ... */ }
var userstate = ttlmap.New[int64, state](3 * time.Hour)
userstate.AutoExpire()
```

Since it uses the new fancy go generics it needs a recent-ish go
version.
